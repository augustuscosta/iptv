package otgmobile.com.br.iptv.service;

import android.content.Context;

import com.j256.ormlite.table.TableUtils;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.rest.RestService;

import java.sql.SQLException;

import otgmobile.com.br.iptv.cloud.AcessoCloud;
import otgmobile.com.br.iptv.cloud.ErrorHandler;
import otgmobile.com.br.iptv.database.DatabaseAccessProvider;
import otgmobile.com.br.iptv.model.Acesso;

/**
 * Created by augustuscosta on 07/07/15.
 */
@EBean
public class AcessoService {

    @RootContext
    protected Context context;

    @RestService
    protected AcessoCloud acessoCloud;

    @Bean
    ErrorHandler errorHandler;

    private DatabaseAccessProvider provider;

    @AfterInject
    void afterInject() {
        acessoCloud.setRestErrorHandler(errorHandler);
    }

    public void autenticar(String username, String password){
        Acesso acesso = acessoCloud.autenticar(username, password);

        if (acesso != null) {
            try {
                provider().acessoDao().createOrUpdate(acesso);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    public void limparToken(){
        try{
            TableUtils.clearTable(provider().getConnectionDataSource(),Acesso.class);
        }catch (SQLException e){
            e.printStackTrace();
        }
    }


    public String getToken(){
        try {
            Acesso acesso = provider().acessoDao().queryBuilder().queryForFirst();
            if(acesso == null){
                return null;
            }
            return acesso.getToken();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void clearDatabase(){
        provider().clearDatabase();
    }

    private DatabaseAccessProvider provider(){
        if(provider == null){
            provider = new DatabaseAccessProvider(context);
        }

        return provider;
    }

}
