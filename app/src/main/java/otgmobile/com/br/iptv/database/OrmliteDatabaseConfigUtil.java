package otgmobile.com.br.iptv.database;

import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import otgmobile.com.br.iptv.model.Acesso;
import otgmobile.com.br.iptv.model.Conteudo;
import otgmobile.com.br.iptv.model.Curso;
import otgmobile.com.br.iptv.model.Modulo;
import otgmobile.com.br.iptv.model.Topico;
import otgmobile.com.br.iptv.model.Usuario;

/**
 * Created by augustuscosta on 29/06/15.
 */
public class OrmliteDatabaseConfigUtil extends OrmLiteConfigUtil {

    private static final Class<?>[] classes = new Class[] {Acesso.class, Usuario.class, Curso.class, Topico.class, Modulo.class, Conteudo.class};

    public static void main(String[] args) throws IOException, SQLException {

        String currDirectory = "user.dir";

        String configPath = "/app/src/main/res/raw/ormlite_config.txt";

        /**
         * Gets the project root directory
         */
        String projectRoot = System.getProperty(currDirectory);

        /**
         * Full configuration path includes the project root path, and the location
         * of the ormlite_config.txt file appended to it
         */
        String fullConfigPath = projectRoot + configPath;

        File configFile = new File(fullConfigPath);

        /**
         * In the a scenario where we run this program serveral times, it will recreate the
         * configuration file each time with the updated configurations.
         */
        if(configFile.exists()) {
            configFile.delete();
            configFile = new File(fullConfigPath);
        }

        /**
         * writeConfigFile is a util method used to write the necessary configurations
         * to the ormlite_config.txt file.
         */
        writeConfigFile(configFile, classes);
    }
}
