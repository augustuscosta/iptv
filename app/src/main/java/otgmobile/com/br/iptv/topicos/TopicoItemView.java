package otgmobile.com.br.iptv.topicos;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.loopj.android.image.SmartImageView;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import otgmobile.com.br.iptv.R;
import otgmobile.com.br.iptv.TopicoDetalheActivity_;
import otgmobile.com.br.iptv.model.Curso;
import otgmobile.com.br.iptv.model.Topico;
import otgmobile.com.br.iptv.util.Session;

/**
 * Created by augustuscosta on 12/08/15.
 */
@EViewGroup(R.layout.topicos_list_item)
public class TopicoItemView extends LinearLayout {

    @ViewById
    TextView nameTextField;

    @ViewById
    SmartImageView thumbImageView;

    Topico topico;

    Curso curso;

    String token;


    public TopicoItemView(Context context) {
        super(context);
    }

    public void bind(Topico topico, Curso curso, String token) {
        this.topico = topico;
        this.token = token;
        this.curso = curso;
        nameTextField.setText(topico.getName());
        if(topico.getThumbImageUrl() != null){
            thumbImageView.setImageUrl(topico.getThumbImageUrl() + "&token=" + token);
        }
    }

    @Click(R.id.compartilharButton)
    void compartilharButtonClick(){
        String shareBody = topico.getCurso().getShortname() + " " + topico.getName();
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "\n\n");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
        getContext().startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.activity_topicos_share)));
    }

    @Click(R.id.playImageButton)
    void playImageButtonButtonClick(){
        if(topico.getStreamUrl() == null)
            return;
        setStreamServer();
        String action = "otgmobile.com.br.iptv.PLAY";
        Intent intent = new Intent();
        intent.setAction(action);
        getContext().sendBroadcast(intent);
    }

    void setStreamServer(){
        Session.setServer(topico.getStreamUrl(),getContext());
    }

    @Click(R.id.infoButton)
    void infoButtonClick(){
        TopicoDetalheActivity_.intent(getContext()).topicoId(topico.getId()).cursoId(curso.getId()).start() ;
    }
}
