package otgmobile.com.br.iptv.cursos;

import android.content.Context;
import android.content.Intent;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import otgmobile.com.br.iptv.CursoDetalheActivity_;
import otgmobile.com.br.iptv.R;
import otgmobile.com.br.iptv.model.Curso;

/**
 * Created by augustuscosta on 12/08/15.
 */
@EViewGroup(R.layout.cursos_list_item)
public class CursoItemView extends LinearLayout {

    @ViewById
    TextView shortnameTextField;

    @ViewById
    TextView fullnameTextField;

    @ViewById
    WebView summaryWebView;

    Curso curso;


    public CursoItemView(Context context) {

        super(context);
    }

    public void bind(Curso curso) {
        this.curso = curso;
        shortnameTextField.setText(curso.getShortname());
        fullnameTextField.setText(curso.getFullname());
        summaryWebView.loadData(curso.getSummary(), "text/html; charset=utf-8", null);
        summaryWebView.setClickable(false);
        summaryWebView.setLongClickable(false);
        summaryWebView.setFocusable(false);
        summaryWebView.setFocusableInTouchMode(false);
    }

    @Click(R.id.compartilharButton)
    void compartilharButtonClick(){
        String shareBody = curso.getShortname() + " " + curso.getFullname();
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "\n\n");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        getContext().startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.activity_cursos_share)));

    }

    @Click(R.id.playButton)
    void playButtonClick(){
        CursoDetalheActivity_.intent(getContext()).cursoId(curso.getId()).start() ;
    }
}
