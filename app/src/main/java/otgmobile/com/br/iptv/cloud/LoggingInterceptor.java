package otgmobile.com.br.iptv.cloud;

import android.util.Log;

import org.androidannotations.annotations.EBean;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by augustuscosta on 10/08/15.
 */
@EBean
public class LoggingInterceptor implements ClientHttpRequestInterceptor {

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        ClientHttpResponse response = execution.execute(request, body);

       // String responseString = stringOf(response.getBody());
       // Log.d("RESPONSE", responseString);

        return response;
    }

    public static String stringOf(InputStream inputStream) {
        inputStream.mark(Integer.MAX_VALUE);
        BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder strBuilder = new StringBuilder();
        String line;
        try {
            while ((line = r.readLine()) != null)
                strBuilder.append(line);
        } catch (IOException ignored) {}
        try {
            inputStream.reset();
        } catch (IOException ignored) {}
        return strBuilder.toString();
    }
}
