package otgmobile.com.br.iptv;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.widget.ListView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.Receiver;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import otgmobile.com.br.iptv.cursos.CursoListAdapter;
import otgmobile.com.br.iptv.model.Curso;
import otgmobile.com.br.iptv.service.AcessoService;
import otgmobile.com.br.iptv.service.CursoService;
import otgmobile.com.br.iptv.util.Session;

@EActivity(R.layout.activity_cursos)
public class CursosActivity extends IPTVActivity {

    @ViewById
    ListView listView;

    @Bean(CursoService.class)
    CursoService cursoService;

    @Bean(AcessoService.class)
    AcessoService acessoService;

    @Bean(CursoListAdapter.class)
    CursoListAdapter cursorAdapter;

    @AfterInject
    void afterCreate(){
        adjustScreen();
        getRemoteCursos();
    }

    @AfterViews
    void afterView(){
        adjustScreen();
    }

    @Override
    protected void onResume() {
        super.onResume();
        listView.setAdapter(cursorAdapter);
    }

    @Background
    void getRemoteCursos(){
        cursoService.getRemoteCursos();
        refreshAdapter();
    }

    @UiThread
    void refreshAdapter(){

        cursorAdapter.refresh();
    }

    @Click(R.id.backButton)
    void backButtonClick(){
        adjustScreen();
        finish();
    }

    @Click(R.id.logoutButton)
    void logoutButtonClick(){
        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.activity_cursos_logout_title))
                .setMessage(getString(R.string.activity_cursos_logout_description))
                .setPositiveButton(R.string.sair, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        acessoService.limparToken();
                        Session.setServer(null,CursosActivity.this);
                        finish();
                    }
                })
                .setNegativeButton(R.string.cancelar, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .show();

    }

    @ItemClick
    void listViewItemClicked(Curso curso) {
        Toast.makeText(this, curso.getFullname(), Toast.LENGTH_SHORT).show();
    }

    @Receiver(actions = "otgmobile.com.br.iptv.PLAY")
    protected void playBroadcastReceiver() {
        finish();
    }

}
