package otgmobile.com.br.iptv.topicos;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.ArrayList;
import java.util.List;

import otgmobile.com.br.iptv.model.Curso;
import otgmobile.com.br.iptv.model.Topico;
import otgmobile.com.br.iptv.service.AcessoService;
import otgmobile.com.br.iptv.service.ModuloService;
import otgmobile.com.br.iptv.service.TopicoService;

/**
 * Created by augustuscosta on 12/08/15.
 */
@EBean
public class TopicoListAdapter extends BaseAdapter {

    @RootContext
    protected Context context;

    Curso curso;
    List<Topico> topicos = new ArrayList<>();

    @Bean
    TopicoService topicoService;

    @Bean
    ModuloService moduloService;

    @Bean
    AcessoService acessoService;


    public void refresh(){
        if(curso != null)
            topicos = (ArrayList)topicoService.getTopicos(curso);
        getModules();
        notifyDataSetChanged();
    }

    private void getModules(){
        if(topicos == null)
            return;
        for(Topico topico:topicos){
            topico.setModules(moduloService.getModulos(topico));
        }
    }

    public void setCurso(Curso curso){
        this.curso = curso;
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        TopicoItemView itemView;
        if (convertView == null) {
            itemView = TopicoItemView_.build(context);
        } else {
            itemView = (TopicoItemView) convertView;
        }

        itemView.bind(getItem(position),curso, acessoService.getToken());


        return itemView;
    }

    @Override
    public int getCount() {
        return topicos.size();
    }

    @Override
    public Topico getItem(int position) {
        return topicos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}
