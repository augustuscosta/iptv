package otgmobile.com.br.iptv.service;

import android.content.Context;

import com.j256.ormlite.stmt.QueryBuilder;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.rest.RestService;

import java.sql.SQLException;
import java.util.Collection;

import otgmobile.com.br.iptv.cloud.ErrorHandler;
import otgmobile.com.br.iptv.cloud.TopicoCloud;
import otgmobile.com.br.iptv.database.DatabaseAccessProvider;
import otgmobile.com.br.iptv.model.Conteudo;
import otgmobile.com.br.iptv.model.Curso;
import otgmobile.com.br.iptv.model.Modulo;
import otgmobile.com.br.iptv.model.Topico;

/**
 * Created by augustuscosta on 19/07/15.
 */
@EBean
public class TopicoService {

    @RootContext
    protected Context context;

    @RestService
    protected TopicoCloud topicoCloud;

    @Bean
    AcessoService acessoService;

    @Bean
    ModuloService moduloService;

    @Bean
    ErrorHandler errorHandler;

    private DatabaseAccessProvider provider;

    @AfterInject
    void afterInject() {
        topicoCloud.setRestErrorHandler(errorHandler);
    }

    public void getRemoteTopicos(Curso curso){
        Collection<Topico> topicos = topicoCloud.getTopicos(acessoService.getToken(), curso.getId().intValue());
        if (topicos != null) {
            try {
                for(Topico topico:topicos){
                    tratarRelacionamentos(curso, topico);
                    provider().topicoDao().createOrUpdate(topico);
                    moduloService.createOrUpdate(topico.getModules());
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private void tratarRelacionamentos(Curso curso, Topico topico) throws SQLException{
        topico.setCurso(curso);
        if(topico.getModules() != null){
            for(Modulo modulo:topico.getModules()){
                modulo.setTopico(topico);
                apagarConteudosDoModulo(modulo);
                if(modulo.getContents() != null){
                    for(Conteudo conteudo:modulo.getContents())
                        conteudo.setModulo(modulo);
                }
            }
        }
    }

    private void apagarConteudosDoModulo(Modulo modulo) throws SQLException{
        Modulo toClean = provider().moduloDao().queryForId(modulo.getId());
        if(toClean == null)
            return;
        if(toClean.getContents() == null)
            return;
        provider().conteudoDao().delete(toClean.getContents());
    }

    public Collection<Topico> getTopicos(Curso curso){
        try {
            QueryBuilder<Curso, Long> queryCurso = provider().cursoDao().queryBuilder();
            queryCurso.where().ge("id", curso.getId());
            QueryBuilder<Topico, Long> queryTopico = provider().topicoDao().queryBuilder();
            Collection<Topico> results = queryTopico.join(queryCurso).query();
            return results;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Topico getTopico(Long id){
        try {
            return provider().topicoDao().queryForId(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    private DatabaseAccessProvider provider(){
        if(provider == null){
            provider = new DatabaseAccessProvider(context);
        }

        return provider;
    }
}
