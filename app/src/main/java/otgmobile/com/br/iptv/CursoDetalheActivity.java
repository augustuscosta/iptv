package otgmobile.com.br.iptv;

import android.widget.ListView;
import android.widget.TextView;

import org.androidannotations.annotations.AfterExtras;
import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.Receiver;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import otgmobile.com.br.iptv.model.Curso;
import otgmobile.com.br.iptv.service.CursoService;
import otgmobile.com.br.iptv.service.TopicoService;
import otgmobile.com.br.iptv.topicos.TopicoListAdapter;

@EActivity(R.layout.activity_curso_detalhe)
public class CursoDetalheActivity extends IPTVActivity {

    @Extra
    Long cursoId;

    @ViewById
    TextView tittleTextView;

    Curso curso;

    @Bean(CursoService.class)
    CursoService cursoService;

    @Bean(TopicoService.class)
    TopicoService topicoService;

    @ViewById
    ListView listView;

    @Bean(TopicoListAdapter.class)
    TopicoListAdapter topicoListAdapter;

    @AfterExtras
    void afterExtra() {
        getCurso();
    }

    @AfterInject
    void afterCreate(){
        adjustScreen();
        getRemoteCursoDetails();
    }

    @AfterViews
    void afterViews(){
        adjustScreen();
        setCursoData();
        refreshUi();
    }

    @Override
    protected void onResume() {
        super.onResume();
        listView.setAdapter(topicoListAdapter);
    }

    void setCursoData(){
        tittleTextView.setText(curso.getShortname());
    }


    void getCurso(){
        curso = cursoService.getCurso(cursoId);
    }

    @Click(R.id.backButton)
    void backButtonClick(){
        adjustScreen();
        finish();
    }

    @Background
    void getRemoteCursoDetails(){
        topicoService.getRemoteTopicos(curso);
        refreshUiThread();
    }

    @UiThread
    void refreshUiThread(){
        refreshUi();
    }

    void refreshUi(){
        setCursoData();
        topicoListAdapter.setCurso(curso);
        topicoListAdapter.refresh();
    }

    @Receiver(actions = "otgmobile.com.br.iptv.PLAY")
    protected void playBroadcastReceiver() {
        finish();
    }
}
