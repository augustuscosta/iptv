package otgmobile.com.br.iptv.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by augustuscosta on 15/07/15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@DatabaseTable
public class Topico {

    @DatabaseField(id=true)
    private Long id;

    @DatabaseField
    private String name;

    @DatabaseField
    private Integer visible;

    @DatabaseField
    private String summary;

    @DatabaseField
    private Integer summaryformat;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    private Curso curso;

    @ForeignCollectionField(eager = true)
    private Collection<Modulo> modules;

    public String getStreamUrl(){
        if(modules == null)
            return null;
        List<Modulo> modulosList = new ArrayList<Modulo>(modules);;
        if(modulosList.size() >= 3){
            Modulo modulo = modulosList.get(2);
            if(modulo.getContents() != null){
                Collection<Conteudo> conteudos = modulo.getContents();
                if(!conteudos.isEmpty()){
                    for(Conteudo conteudo:conteudos){
                        return conteudo.getFileurl();
                    }
                }
            }
        }
        return null;
    }

    public String getMediumImageUrl(){
        if(modules == null)
            return null;
        List<Modulo> modulosList = new ArrayList<Modulo>(modules);;
        if(modulosList.size() >= 2){
            Modulo modulo = modulosList.get(1);
            if(modulo.getContents() != null){
                Collection<Conteudo> conteudos = modulo.getContents();
                if(!conteudos.isEmpty()){
                    for(Conteudo conteudo:conteudos){
                        return conteudo.getFileurl();
                    }
                }
            }
        }
        return null;
    }

    public String getThumbImageUrl(){
        if(modules == null)
            return null;
        List<Modulo> modulosList = new ArrayList<Modulo>(modules);;
        if(modulosList.size() >= 1){
            Modulo modulo = modulosList.get(0);
            if(modulo.getContents() != null){
                Collection<Conteudo> conteudos = modulo.getContents();
                if(!conteudos.isEmpty()){
                    for(Conteudo conteudo:conteudos){
                        return conteudo.getFileurl();
                    }
                }
            }
        }
        return null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getVisible() {
        return visible;
    }

    public void setVisible(Integer visible) {
        this.visible = visible;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Integer getSummaryformat() {
        return summaryformat;
    }

    public void setSummaryformat(Integer summaryformat) {
        this.summaryformat = summaryformat;
    }

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

    public Collection<Modulo> getModules() {
        return modules;
    }

    public void setModules(Collection<Modulo> modules) {
        this.modules = modules;
    }
}
