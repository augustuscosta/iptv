package otgmobile.com.br.iptv.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by augustuscosta on 15/07/15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@DatabaseTable
public class Usuario {

    @DatabaseField(generatedId=true, allowGeneratedIdInsert=true)
    private Long id;

    @DatabaseField
    private String sitename;

    @DatabaseField
    private String username;

    @DatabaseField
    private String firstname;

    @DatabaseField
    private String lastname;

    @DatabaseField
    private String fullname;

    @DatabaseField
    private String lang;

    @DatabaseField
    private Long userid;

    @DatabaseField
    private String siteurl;

    @DatabaseField
    private String userpictureurl;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSitename() {
        return sitename;
    }

    public void setSitename(String sitename) {
        this.sitename = sitename;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    public String getSiteurl() {
        return siteurl;
    }

    public void setSiteurl(String siteurl) {
        this.siteurl = siteurl;
    }

    public String getUserpictureurl() {
        return userpictureurl;
    }

    public void setUserpictureurl(String userpictureurl) {
        this.userpictureurl = userpictureurl;
    }
}
