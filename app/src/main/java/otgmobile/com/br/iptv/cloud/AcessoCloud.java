package otgmobile.com.br.iptv.cloud;

import org.androidannotations.annotations.rest.Get;
import org.androidannotations.annotations.rest.Rest;
import org.androidannotations.api.rest.RestClientErrorHandling;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import otgmobile.com.br.iptv.model.Acesso;

/**
 * Created by augustuscosta on 07/07/15.
 */
@Rest(rootUrl = "http://200.17.34.31/moodle/login", converters = { MappingJackson2HttpMessageConverter.class, StringHttpMessageConverter.class })
public interface AcessoCloud extends RestClientErrorHandling {

    @Get("/token.php?service=moodle_mobile_app&username={username}&password={password}")
    Acesso autenticar(String username, String password);

}
