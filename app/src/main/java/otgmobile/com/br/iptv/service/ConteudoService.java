package otgmobile.com.br.iptv.service;

import android.content.Context;

import com.j256.ormlite.stmt.QueryBuilder;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.sql.SQLException;
import java.util.Collection;

import otgmobile.com.br.iptv.database.DatabaseAccessProvider;
import otgmobile.com.br.iptv.model.Conteudo;
import otgmobile.com.br.iptv.model.Modulo;

/**
 * Created by augustuscosta on 20/07/15.
 */
@EBean
public class ConteudoService {

    @RootContext
    protected Context context;

    private DatabaseAccessProvider provider;


    public Collection<Conteudo> getConteudos(Modulo modulo){
        try {
            QueryBuilder<Modulo, Long> queryModulo = provider().moduloDao().queryBuilder();
            queryModulo.where().ge("id", modulo.getId());
            QueryBuilder<Conteudo, Long> queryConteudo = provider().conteudoDao().queryBuilder();
            Collection<Conteudo> results = queryConteudo.join(queryModulo).query();
            return results;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void createOrUpdate(Collection<Conteudo> conteudos) throws SQLException{
        for(Conteudo conteudo:conteudos)
            provider().conteudoDao().createOrUpdate(conteudo);
    }

    private DatabaseAccessProvider provider(){
        if(provider == null){
            provider = new DatabaseAccessProvider(context);
        }

        return provider;
    }
}
