package otgmobile.com.br.iptv.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

/**
 * Created by augustuscosta on 15/07/15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@DatabaseTable
public class Curso {

    @DatabaseField(id=true)
    private Long id;

    @DatabaseField
    private String shortname;

    @DatabaseField
    private String fullname;

    @DatabaseField
    private Integer enrolledusercount;

    @DatabaseField
    private String idnumber;

    @DatabaseField
    private Integer visible;

    @DatabaseField
    //HTML TEXTO
    private String summary;

    @DatabaseField
    private Integer summaryformat;

    @DatabaseField
    private String format;

    @DatabaseField
    private Boolean showgrades;

    @DatabaseField
    private String lang;

    @DatabaseField
    private Boolean enablecompletion;

    @ForeignCollectionField(eager = true)
    private Collection<Topico> topicos;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getShortname() {
        return shortname;
    }

    public void setShortname(String shortname) {
        this.shortname = shortname;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public Integer getEnrolledusercount() {
        return enrolledusercount;
    }

    public void setEnrolledusercount(Integer enrolledusercount) {
        this.enrolledusercount = enrolledusercount;
    }

    public String getIdnumber() {
        return idnumber;
    }

    public void setIdnumber(String idnumber) {
        this.idnumber = idnumber;
    }

    public Integer getVisible() {
        return visible;
    }

    public void setVisible(Integer visible) {
        this.visible = visible;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Integer getSummaryformat() {
        return summaryformat;
    }

    public void setSummaryformat(Integer summaryformat) {
        this.summaryformat = summaryformat;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public Boolean getShowgrades() {
        return showgrades;
    }

    public void setShowgrades(Boolean showgrades) {
        this.showgrades = showgrades;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public Boolean getEnablecompletion() {
        return enablecompletion;
    }

    public void setEnablecompletion(Boolean enablecompletion) {
        this.enablecompletion = enablecompletion;
    }

    public Collection<Topico> getTopicos() {
        return topicos;
    }

    public void setTopicos(Collection<Topico> topicos) {
        this.topicos = topicos;
    }
}
