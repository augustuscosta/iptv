package otgmobile.com.br.iptv.service;

import android.content.Context;

import com.j256.ormlite.stmt.QueryBuilder;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.sql.SQLException;
import java.util.Collection;

import otgmobile.com.br.iptv.database.DatabaseAccessProvider;
import otgmobile.com.br.iptv.model.Modulo;
import otgmobile.com.br.iptv.model.Topico;

/**
 * Created by augustuscosta on 20/07/15.
 */
@EBean
public class ModuloService {


    @RootContext
    protected Context context;

    @Bean
    ConteudoService conteudoService;

    private DatabaseAccessProvider provider;


    public Collection<Modulo> getModulos(Topico topico){
        try {
            QueryBuilder<Topico, Long> queryTopico = provider().topicoDao().queryBuilder();
            queryTopico.where().ge("id", topico.getId());
            QueryBuilder<Modulo, Long> queryModulo = provider().moduloDao().queryBuilder();
            Collection<Modulo> results = queryModulo.join(queryTopico).query();
            return results;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void createOrUpdate(Collection<Modulo> modulos) throws SQLException{
        for(Modulo modulo:modulos){
            provider().moduloDao().createOrUpdate(modulo);
            conteudoService.createOrUpdate(modulo.getContents());
        }

    }

    private DatabaseAccessProvider provider(){
        if(provider == null){
            provider = new DatabaseAccessProvider(context);
        }

        return provider;
    }
}
