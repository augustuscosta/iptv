package otgmobile.com.br.iptv;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import otgmobile.com.br.iptv.service.AcessoService;
import otgmobile.com.br.iptv.service.UsuarioService;

@EActivity(R.layout.activity_login)
public class LoginActivity extends IPTVActivity {

    ProgressDialog pDialog;

    @Bean(AcessoService.class)
    AcessoService acessoService;

    @Bean(UsuarioService.class)
    UsuarioService usuarioService;

    @ViewById
    EditText usuarioEditText;

    @ViewById
    EditText passwordEditText;

    @AfterInject
    void afterCreate(){
        adjustScreen();
    }

    @Click(R.id.backButton)
    void backButtonClick(){
        adjustScreen();
        finish();
    }

    @Click(R.id.confirmButton)
    void confirmButtonClick(){
        if(validate()){
            acessar(usuarioEditText.getText().toString(), passwordEditText.getText().toString());
        }
    }

    @Background
    void acessar(String username, String password){
        showProgressDialog();
        acessoService.autenticar(username, password);
        String token = acessoService.getToken();
        if(token != null){
            usuarioService.getRemoteUsuario();
        }

        hideProgressDialog();
        checkLogin();
    }

    @UiThread
    void checkLogin(){
        String token = acessoService.getToken();
        if(token == null){
            showAcessoNaoRealizado();
        }else{
            finish();
        }

    }

    @UiThread
    void showProgressDialog(){
        pDialog = new ProgressDialog(this);
        pDialog.setMessage(getString(R.string.activity_login_progress_dialog_text));
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    @UiThread
    void hideProgressDialog(){
        pDialog.dismiss();
    }

    private void showAcessoNaoRealizado(){
        new AlertDialog.Builder(this)
                .setTitle(R.string.activity_login_dialog_tittle)
                .setMessage(R.string.activity_login_dialog_text)
                .setNegativeButton(R.string.activity_login_dialog_back, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                })
                .show();
    }

    private boolean validate(){
        boolean send = true;

        if(!isValidUsuario(usuarioEditText.getText().toString())){
            send = false;
            usuarioEditText.setError(getString(R.string.activity_login_username_invalid));
        }

        if(!isValidPassword(passwordEditText.getText().toString())){
            send = false;
            passwordEditText.setError(getString(R.string.activity_login_password_invalid));
        }

        return send;
    }

    // validating password with retype password
    private boolean isValidPassword(String pass) {
        if (pass != null && pass.length() > 4) {
            return true;
        }
        return false;
    }

    // validating password with retype password
    private boolean isValidUsuario(String user) {
        if (user != null && user.length() > 4) {
            return true;
        }
        return false;
    }

}
