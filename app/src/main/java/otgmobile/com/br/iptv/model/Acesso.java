package otgmobile.com.br.iptv.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by augustuscosta on 07/07/15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@DatabaseTable
public class Acesso {

    @DatabaseField(generatedId=true)
    private Long id;

    @DatabaseField
    private String token;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
