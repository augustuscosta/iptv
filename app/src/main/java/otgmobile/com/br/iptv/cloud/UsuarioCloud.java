package otgmobile.com.br.iptv.cloud;

import org.androidannotations.annotations.rest.Get;
import org.androidannotations.annotations.rest.Rest;
import org.androidannotations.api.rest.RestClientErrorHandling;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import otgmobile.com.br.iptv.model.Usuario;

/**
 * Created by augustuscosta on 15/07/15.
 */
@Rest(rootUrl = "http://200.17.34.31/moodle/webservice/rest/server.php", converters = MappingJackson2HttpMessageConverter.class)
public interface UsuarioCloud extends RestClientErrorHandling {

    @Get("?wsfunction=core_webservice_get_site_info&moodlewsrestformat=json&wstoken={wstoken}")
    Usuario getUsuario(String wstoken);
}
