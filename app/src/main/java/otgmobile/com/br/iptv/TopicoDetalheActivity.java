package otgmobile.com.br.iptv;

import android.content.Intent;
import android.webkit.WebView;
import android.widget.TextView;

import com.loopj.android.image.SmartImageView;

import org.androidannotations.annotations.AfterExtras;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.Receiver;
import org.androidannotations.annotations.ViewById;

import otgmobile.com.br.iptv.model.Curso;
import otgmobile.com.br.iptv.model.Topico;
import otgmobile.com.br.iptv.service.AcessoService;
import otgmobile.com.br.iptv.service.CursoService;
import otgmobile.com.br.iptv.service.TopicoService;
import otgmobile.com.br.iptv.util.Session;

@EActivity(R.layout.activity_topico_detalhe)
public class TopicoDetalheActivity extends IPTVActivity {

    @ViewById
    TextView tittleTextView;

    @ViewById
    SmartImageView mediumImageView;

    @ViewById
    WebView summaryWebView;

    @ViewById
    TextView topicoTextView;

    @Extra
    Long cursoId;

    @Extra
    Long topicoId;

    Curso curso;

    Topico topico;

    @Bean(CursoService.class)
    CursoService cursoService;

    @Bean(TopicoService.class)
    TopicoService topicoService;

    @Bean(AcessoService.class)
    AcessoService acessoService;

    @AfterExtras
    void afterExtra() {
        getCurso();
        getTopico();
    }

    void getCurso(){
        curso = cursoService.getCurso(cursoId);
    }

    void getTopico(){
        topico = topicoService.getTopico(topicoId);
    }

    @AfterViews
    void afterViews(){
        adjustScreen();
        setTopicoData();
    }

    void setTopicoData(){
        tittleTextView.setText(curso.getShortname());
        topicoTextView.setText(topico.getName());
        if(topico.getThumbImageUrl() != null){
            mediumImageView.setImageUrl(topico.getThumbImageUrl() + "&token=" + acessoService.getToken());
        }
        summaryWebView.loadData(topico.getSummary(), "text/html; charset=utf-8", null);
        summaryWebView.setClickable(false);
        summaryWebView.setLongClickable(false);
        summaryWebView.setFocusable(false);
        summaryWebView.setFocusableInTouchMode(false);
    }

    @Click(R.id.compartilharButton)
    void compartilharButtonClick(){
        String shareBody = topico.getCurso().getShortname() + " " + topico.getName();
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "\n\n");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.activity_topicos_share)));
    }

    @Click(R.id.playImageButton)
    void playImageButtonButtonClick(){
        if(topico.getStreamUrl() == null)
            return;
        setStreamServer();
        String action = "otgmobile.com.br.iptv.PLAY";
        Intent intent = new Intent();
        intent.setAction(action);
        sendBroadcast(intent);
    }

    @Click(R.id.backButton)
    void backButtonClick(){
        adjustScreen();
        finish();
    }

    void setStreamServer(){
        Session.setServer(topico.getStreamUrl(), this);
    }

    @Receiver(actions = "otgmobile.com.br.iptv.PLAY")
    protected void playBroadcastReceiver() {
        finish();
    }

}
