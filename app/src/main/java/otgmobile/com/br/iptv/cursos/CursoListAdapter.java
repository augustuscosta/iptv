package otgmobile.com.br.iptv.cursos;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import otgmobile.com.br.iptv.database.DatabaseAccessProvider;
import otgmobile.com.br.iptv.model.Curso;

/**
 * Created by augustuscosta on 12/08/15.
 */
@EBean
public class CursoListAdapter extends BaseAdapter {

    @RootContext
    protected Context context;

    List<Curso> cursos;

    private DatabaseAccessProvider provider;

    @AfterInject
    void initAdapter() {
        refresh();
    }

    public void refresh(){
        try {
            cursos = provider().cursoDao().queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        notifyDataSetChanged();
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        CursoItemView itemView;
        if (convertView == null) {
            itemView = CursoItemView_.build(context);
        } else {
            itemView = (CursoItemView) convertView;
        }

        itemView.bind(getItem(position));

        return itemView;
    }

    @Override
    public int getCount() {
        return cursos.size();
    }

    @Override
    public Curso getItem(int position) {
        return cursos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private DatabaseAccessProvider provider(){
        if(provider == null){
            provider = new DatabaseAccessProvider(context);
        }

        return provider;
    }
}
