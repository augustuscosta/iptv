package otgmobile.com.br.iptv.database;

import android.content.Context;
import android.util.Log;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import otgmobile.com.br.iptv.model.Acesso;
import otgmobile.com.br.iptv.model.Conteudo;
import otgmobile.com.br.iptv.model.Curso;
import otgmobile.com.br.iptv.model.Modulo;
import otgmobile.com.br.iptv.model.Topico;
import otgmobile.com.br.iptv.model.Usuario;

/**
 * Created by augustuscosta on 08/07/15.
 */
public class DatabaseAccessProvider {
    private DatabaseHelper databaseHelper;
    private Dao<Acesso, Long> acessoDao;
    private Dao<Usuario, Long> usuarioDao;
    private Dao<Curso, Long> cursoDao;
    private Dao<Topico, Long> topicoDao;
    private Dao<Modulo, Long> moduloDao;
    private Dao<Conteudo, Long> conteudoDao;

    public DatabaseAccessProvider(Context context){
        databaseHelper = (DatabaseHelper) DatabaseHelper.getDatabase(context);
    }

    public ConnectionSource getConnectionDataSource(){
        return databaseHelper.getConnectionSource();
    }


    public Dao<Acesso, Long> acessoDao(){
        if(acessoDao == null){
            try {
                acessoDao = databaseHelper.getDao(Acesso.class);
            } catch (Exception e) {
                Log.e("Acesso", "Erro criando o DAO ACESSO");
            }
        }
        return acessoDao;
    }

    public Dao<Usuario, Long> usuarioDao(){
        if(usuarioDao == null){
            try {
                usuarioDao = databaseHelper.getDao(Usuario.class);
            } catch (Exception e) {
                Log.e("Usuario", "Erro criando o DAO USUARIO");
            }
        }
        return usuarioDao;
    }

    public Dao<Curso, Long> cursoDao(){
        if(cursoDao == null){
            try {
                cursoDao = databaseHelper.getDao(Curso.class);
            } catch (Exception e) {
                Log.e("Curso", "Erro criando o DAO CURSO");
            }
        }
        return cursoDao;
    }

    public Dao<Topico, Long> topicoDao(){
        if(topicoDao == null){
            try {
                topicoDao = databaseHelper.getDao(Topico.class);
            } catch (Exception e) {
                Log.e("Topico", "Erro criando o DAO TOPICO");
            }
        }
        return topicoDao;
    }

    public Dao<Modulo, Long> moduloDao(){
        if(moduloDao == null){
            try {
                moduloDao = databaseHelper.getDao(Modulo.class);
            } catch (Exception e) {
                Log.e("Modulo", "Erro criando o DAO MODULO");
            }
        }
        return moduloDao;
    }

    public Dao<Conteudo, Long> conteudoDao(){
        if(conteudoDao == null){
            try {
                conteudoDao = databaseHelper.getDao(Conteudo.class);
            } catch (Exception e) {
                Log.e("Conteudo", "Erro criando o DAO Conteudo");
            }
        }
        return conteudoDao;
    }

    public void clearDatabase(){
        try {
            TableUtils.clearTable(getConnectionDataSource(),Acesso.class);
            TableUtils.clearTable(getConnectionDataSource(),Usuario.class);
            TableUtils.clearTable(getConnectionDataSource(),Curso.class);
            TableUtils.clearTable(getConnectionDataSource(),Topico.class);
            TableUtils.clearTable(getConnectionDataSource(),Modulo.class);
            TableUtils.clearTable(getConnectionDataSource(),Conteudo.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}
