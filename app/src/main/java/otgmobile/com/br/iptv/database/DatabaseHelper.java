package otgmobile.com.br.iptv.database;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import otgmobile.com.br.iptv.R;
import otgmobile.com.br.iptv.model.Acesso;
import otgmobile.com.br.iptv.model.Conteudo;
import otgmobile.com.br.iptv.model.Curso;
import otgmobile.com.br.iptv.model.Modulo;
import otgmobile.com.br.iptv.model.Topico;
import otgmobile.com.br.iptv.model.Usuario;

/**
 * Created by augustuscosta on 29/06/15.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper{

    private static final String DATABASE_NAME = "iptv";
    private static final int DATABASE_VERSION = 1;
    private static DatabaseHelper databaseHelper;


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {

            TableUtils.createTable(connectionSource, Acesso.class);
            TableUtils.createTable(connectionSource, Usuario.class);
            TableUtils.createTable(connectionSource, Curso.class);
            TableUtils.createTable(connectionSource, Topico.class);
            TableUtils.createTable(connectionSource, Modulo.class);
            TableUtils.createTable(connectionSource, Conteudo.class);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource,
                         int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, Acesso.class, false);
            TableUtils.dropTable(connectionSource, Usuario.class, false);
            TableUtils.dropTable(connectionSource, Curso.class, false);
            TableUtils.dropTable(connectionSource, Topico.class, false);
            TableUtils.dropTable(connectionSource, Modulo.class, false);
            TableUtils.dropTable(connectionSource, Conteudo.class, false);
            onCreate(database, connectionSource);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static OrmLiteSqliteOpenHelper getDatabase(Context context){
        if(databaseHelper == null){
            databaseHelper = new DatabaseHelper(context);
        }

        return databaseHelper;
    }

}