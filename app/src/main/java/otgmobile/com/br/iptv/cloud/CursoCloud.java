package otgmobile.com.br.iptv.cloud;

import org.androidannotations.annotations.rest.Get;
import org.androidannotations.annotations.rest.Rest;
import org.androidannotations.api.rest.RestClientErrorHandling;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.util.Collection;

import otgmobile.com.br.iptv.model.Curso;

/**
 * Created by augustuscosta on 17/07/15.
 */
@Rest(rootUrl = "http://200.17.34.31/moodle/webservice/rest", converters = MappingJackson2HttpMessageConverter.class)
public interface CursoCloud extends RestClientErrorHandling {

    @Get("/server.php?wsfunction=core_enrol_get_users_courses&moodlewsrestformat=json&wstoken={wstoken}&userid={userid}")
    Collection<Curso> getCursos(String wstoken, Integer userid);
}
