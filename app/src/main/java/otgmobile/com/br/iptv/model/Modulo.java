package otgmobile.com.br.iptv.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

/**
 * Created by augustuscosta on 17/07/15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@DatabaseTable
public class Modulo {

    @DatabaseField(id=true)
    private Long id;

    @DatabaseField
    private String url;

    @DatabaseField
    private String name;

    @DatabaseField
    private Integer instance;

    @DatabaseField
    private Integer visible;

    @DatabaseField
    private String modicon;

    @DatabaseField
    private String modname;

    @DatabaseField
    private String modplural;

    @DatabaseField
    private Integer indent;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    private Topico topico;

    @ForeignCollectionField(eager = true)
    private Collection<Conteudo> contents;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getInstance() {
        return instance;
    }

    public void setInstance(Integer instance) {
        this.instance = instance;
    }

    public Integer getVisible() {
        return visible;
    }

    public void setVisible(Integer visible) {
        this.visible = visible;
    }

    public String getModicon() {
        return modicon;
    }

    public void setModicon(String modicon) {
        this.modicon = modicon;
    }

    public String getModname() {
        return modname;
    }

    public void setModname(String modname) {
        this.modname = modname;
    }

    public String getModplural() {
        return modplural;
    }

    public void setModplural(String modplural) {
        this.modplural = modplural;
    }

    public Integer getIndent() {
        return indent;
    }

    public void setIndent(Integer indent) {
        this.indent = indent;
    }

    public Topico getTopico() {
        return topico;
    }

    public void setTopico(Topico topico) {
        this.topico = topico;
    }

    public Collection<Conteudo> getContents() {
        return contents;
    }

    public void setContents(Collection<Conteudo> contents) {
        this.contents = contents;
    }
}
