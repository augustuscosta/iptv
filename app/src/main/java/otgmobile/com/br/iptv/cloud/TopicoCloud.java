package otgmobile.com.br.iptv.cloud;

import org.androidannotations.annotations.rest.Get;
import org.androidannotations.annotations.rest.Rest;
import org.androidannotations.api.rest.RestClientErrorHandling;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.util.Collection;

import otgmobile.com.br.iptv.model.Topico;

/**
 * Created by augustuscosta on 18/07/15.
 */
@Rest(rootUrl = "http://200.17.34.31/moodle/webservice/rest", converters = MappingJackson2HttpMessageConverter.class)
public interface TopicoCloud extends RestClientErrorHandling {

    @Get("/server.php?moodlewsrestformat=json&wsfunction=core_course_get_contents&wstoken={wstoken}&courseid={courseid}")
    Collection<Topico> getTopicos(String wstoken, Integer courseid);
}

