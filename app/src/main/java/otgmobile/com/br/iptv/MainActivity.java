package otgmobile.com.br.iptv;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.view.View;
import android.widget.ImageButton;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Receiver;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import de.hdodenhof.circleimageview.CircleImageView;
import otgmobile.com.br.iptv.model.Usuario;
import otgmobile.com.br.iptv.service.AcessoService;
import otgmobile.com.br.iptv.service.UsuarioService;
import otgmobile.com.br.iptv.util.Session;

@EActivity(R.layout.activity_main)
public class MainActivity extends IPTVActivity {

    @ViewById
    VideoView videoView;

    @ViewById
    ImageButton menuButton;

    @ViewById
    TextView usernameTextView;

    @ViewById
    CircleImageView avatarImageView;

    @Bean(AcessoService.class)
    AcessoService acessoService;

    @Bean(UsuarioService.class)
    UsuarioService usuarioService;

    @AfterInject
    void afterCreate(){
        fullScreenActivityMode();
    }

    @AfterViews
    void initStreamPlay() {
        play(Session.getServer(this));
        addMediaController();
        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            public boolean onError(MediaPlayer mp, int what, int extra) {
                mp.start();
                return true;
            }
        });
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                play(Session.getServer(MainActivity.this));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        fullScreenActivityMode();
        checkUserData();
    }

    private void checkUserData(){
        String token = acessoService.getToken();
        if(token != null){
            Usuario usuario = usuarioService.getUsuario();
            if(usuario != null){
                showUserData(usuario);
            }
        }else{
            acessoService.clearDatabase();
            hideUserData();
        }
    }

    private void showUserData(Usuario usuario){
        usernameTextView.setText(usuario.getFirstname());
        usernameTextView.setVisibility(View.VISIBLE);
        getUserAvatar(usuario);
    }

    @Background
    void getUserAvatar(Usuario usuario){
        try {
            Bitmap bitmap = BitmapFactory.decodeStream((InputStream) new URL(usuario.getUserpictureurl()).getContent());
            showUserAvatarImage(bitmap);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @UiThread
    void showUserAvatarImage(Bitmap bitmap){
        avatarImageView.setImageBitmap(bitmap);
        avatarImageView.setVisibility(View.VISIBLE);
    }

    private void hideUserData(){
        avatarImageView.setImageResource(R.mipmap.avatar);
        avatarImageView.setVisibility(View.GONE);
        usernameTextView.setText("");
        usernameTextView.setVisibility(View.GONE);
    }


    @Receiver(actions = "otgmobile.com.br.iptv.PLAY")
    protected void playBroadcastReceiver() {
        initStreamPlay();
    }

    private void play(String url){
        try{
            Uri vidUri = Uri.parse(url);
            videoView.setVideoURI(vidUri);
            videoView.start();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void addMediaController(){
        MediaController vidControl = new MediaController(this);
        vidControl.setAnchorView(videoView);
        videoView.setMediaController(vidControl);
    }

    @Click(R.id.menuButton)
    void menuClick(){
        if(acessoService.getToken() == null){
            opneLoginView();
        }else{
            openCursosView();
        }
    }

    private void opneLoginView(){
        LoginActivity_.intent(this).start();
    }

    private void openCursosView(){
        CursosActivity_.intent(this).start();
    }




}
