package otgmobile.com.br.iptv.service;

import android.content.Context;

import com.j256.ormlite.table.TableUtils;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.rest.RestService;

import java.sql.SQLException;

import otgmobile.com.br.iptv.cloud.ErrorHandler;
import otgmobile.com.br.iptv.cloud.UsuarioCloud;
import otgmobile.com.br.iptv.database.DatabaseAccessProvider;
import otgmobile.com.br.iptv.model.Usuario;

/**
 * Created by augustuscosta on 15/07/15.
 */
@EBean
public class UsuarioService {

    @RootContext
    protected Context context;

    @RestService
    protected UsuarioCloud usuarioCloud;

    @Bean
    AcessoService acessoService;

    @Bean
    ErrorHandler errorHandler;

    private DatabaseAccessProvider provider;

    @AfterInject
    void afterInject() {
        usuarioCloud.setRestErrorHandler(errorHandler);
    }

    public void getRemoteUsuario(){
        Usuario usuario = usuarioCloud.getUsuario(acessoService.getToken());
        if (usuario != null) {
            try {
                provider().usuarioDao().createOrUpdate(usuario);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void limparUsuario(){
        try{
            TableUtils.clearTable(provider().getConnectionDataSource(), Usuario.class);
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    public Usuario getUsuario(){
        try {
            Usuario usuario = provider().usuarioDao().queryBuilder().queryForFirst();
            return usuario;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    private DatabaseAccessProvider provider(){
        if(provider == null){
            provider = new DatabaseAccessProvider(context);
        }

        return provider;
    }
}
