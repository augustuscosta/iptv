package otgmobile.com.br.iptv.service;

import android.content.Context;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.SupposeBackground;
import org.androidannotations.annotations.rest.RestService;

import java.sql.SQLException;
import java.util.Collection;

import otgmobile.com.br.iptv.cloud.CursoCloud;
import otgmobile.com.br.iptv.cloud.ErrorHandler;
import otgmobile.com.br.iptv.database.DatabaseAccessProvider;
import otgmobile.com.br.iptv.model.Curso;

/**
 * Created by augustuscosta on 17/07/15.
 */
@EBean
public class CursoService {

    @RootContext
    protected Context context;

    @RestService
    protected CursoCloud cursoCloud;

    @Bean
    AcessoService acessoService;

    @Bean
    UsuarioService usuarioService;

    @Bean
    ErrorHandler errorHandler;

    private DatabaseAccessProvider provider;

    @AfterInject
    void afterInject() {
        cursoCloud.setRestErrorHandler(errorHandler);
    }

    public void getRemoteCursos(){
        Collection<Curso> cursos = cursoCloud.getCursos(acessoService.getToken(),usuarioService.getUsuario().getUserid().intValue());
        if (cursos != null) {
            try {
                for(Curso curso:cursos)
                    provider().cursoDao().createOrUpdate(curso);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public Collection<Curso> getCuros(){
        try {
            return provider().cursoDao().queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Curso getCurso(Long id){
        try {
            return provider().cursoDao().queryForId(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    private DatabaseAccessProvider provider(){
        if(provider == null){
            provider = new DatabaseAccessProvider(context);
        }

        return provider;
    }
}
