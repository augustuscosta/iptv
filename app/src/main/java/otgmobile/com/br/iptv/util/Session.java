package otgmobile.com.br.iptv.util;

import android.content.Context;
import android.content.SharedPreferences;

import otgmobile.com.br.iptv.R;

/**
 * Created by augustuscosta on 21/07/15.
 */
public class Session {

    public static final String PREFS = "SESSION_PREFS_IPTV";
    public static final String SERVER_PREFS_KEY = "SERVER_PREFS_KEY_IPTV";

    private static SharedPreferences settings;

    private static SharedPreferences getSharedPreferencesInstance(
            Context context) {
        if (settings == null) {
            settings = context
                    .getSharedPreferences(PREFS, Context.MODE_PRIVATE);
        }
        return settings;
    }

    public static void setServer(String server, Context context) {
        settings = getSharedPreferencesInstance(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(SERVER_PREFS_KEY, server);
        editor.commit();
    }

    public static String getServer(Context context) {
        String url = "android.resource://otgmobile.com.br.iptv/" + R.raw.introducao;
        return getSharedPreferencesInstance(context).getString(
                SERVER_PREFS_KEY,
                url);
    }
}
